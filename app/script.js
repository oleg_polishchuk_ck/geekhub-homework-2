$(document).ready(function () {
    $('form').submit(function () {
        var $input = $('#td-input');

        if ($input.val() !== '') {
            $('#td-list').append(
                "<li class='list-group-item'>" +
                "   <a>" +
                "       <span class='glyphicon glyphicon-remove-circle'></span>" +
                "   </a>" +
                "   <input class='toggle' type='checkbox'>  " +
                "   <label data=''>" + $input.val() + "</label>" +
                "</li>");

        }

        $input.val('');
        return false;
    });

    $(document).on('click', 'a', function (e) {
        e.preventDefault();
        $(this).parent().remove();
    });

    $(document).on('click', 'input', function () {
        console.log('check');
        var $label = $(this).closest('li').find('label');

        if ($label.attr('data') == 'done') {
            $label.attr('data', '');
            $label.css('text-decoration', 'none');
        }
        else {
            $label.attr('data', 'done');
            $label.css('text-decoration', 'line-through');
        }
    });
});